import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions } from 'react-native';
import axios from 'axios';

const { width, height } = Dimensions.get('window');

export default class App extends React.Component {
  state = {
    questions: [],
    score: 0,
    currentQuestion: -1,
    startTime: 0
  };

  async componentDidMount(){
    let response = await axios.post('https://opentdb.com/api.php?amount=10');
    this.setState({ questions: response.data.results });
  }

  changeCurrentQuestion(number){
    let currentTime = new Date().getTime() / 1000;
    this.setState({ currentQuestion: number, startTime: currentTime });
  }

  getTotalTime(){
    let startTime = this.state.startTime;
    let currentTime = new Date().getTime() / 1000;
    return Math.round(currentTime - startTime);
  }

  getShuffledAnswers(question){
    let answers = question.incorrect_answers;
    answers.push(question.correct_answer);
    return this.shuffle(answers);
  }

  shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

renderAnswer(answers){
  return answers.answers.map((answer, index) => (
    <TouchableOpacity key={index} style={styles.answerButtonStyle} onPress={() => this.processAnswer(answer)}>
      <Text style={styles.answerText}>{answer}</Text>
    </TouchableOpacity>
));
}

processAnswer(answer){
  let correctAnswer = this.state.questions[this.state.currentQuestion].correct_answer;
  if (correctAnswer === answer) {
    this.setState({ score: this.state.score + 1 });
  }
  this.setState({ currentQuestion: this.state.currentQuestion + 1 });
}

async resetGame(){
  this.setState({
    questions: [],
    score: 0,
    currentQuestion: -1,
    startTime: 0
  });
  let response = await axios.post('https://opentdb.com/api.php?amount=10');
  this.setState({ questions: response.data.results });
}

  render() {
      if(this.state.currentQuestion < 0){
        return (
          <View style={styles.container}>
            <TouchableOpacity disabled={!this.state.questions.length > 0} style={styles.buttonStyle} onPress={() => this.changeCurrentQuestion(0)}>
              <Text style={styles.textStyle}>Start Quiz!</Text>
            </TouchableOpacity>
          </View>
        );
      } else if (this.state.currentQuestion < 10) {
        let answers = this.getShuffledAnswers(this.state.questions[this.state.currentQuestion]);
        return (
          <View style={styles.container}>
            <View style={styles.topView}>
              <Text style={styles.questionStyle}>{this.state.questions[this.state.currentQuestion].question}</Text>
            </View>
            <View style={styles.bottomView}>
                {this.renderAnswer({answers})}
            </View>
          </View>
        );
      } else {
        return (
          <View style={styles.container}>
            <View style={styles.resultViewStyle}>
              <Text style={styles.resultStyle}>Total Score:</Text>
              <Text style={styles.resultStyle}>{this.state.score}</Text>
            </View>
            <View style={styles.resultViewStyle}>
              <Text style={styles.resultStyle}>Total Time (seconds):</Text>
              <Text style={styles.resultStyle}>{this.getTotalTime()}</Text>
            </View>
              <TouchableOpacity style={styles.buttonStyle} onPress={() => this.resetGame()}>
                <Text style={styles.textStyle}>Play Again!</Text>
              </TouchableOpacity>
          </View>
        );
      }
  }
}

const styles = StyleSheet.create({
  topView: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    width: width * 0.8,
  },
  bottomView: {
    flex: 2,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonStyle: {
    backgroundColor: 'grey',
    alignSelf: 'center',
    justifyContent: 'center',
    height: height * 0.1,
    width: width * 0.6,
    borderRadius: 10
  },
  answerButtonStyle: {
    marginTop: height * 0.02,
    backgroundColor: 'grey',
    alignSelf: 'center',
    justifyContent: 'center',
    height: height * 0.07,
    width: width * 0.8,
    borderRadius: 10
  },
  textStyle: {
    color:'white',
    textAlign: 'center',
    fontSize: 20
  },
  resultStyle: {
    color:'black',
    textAlign: 'center',
    fontSize: 20
  },
  resultViewStyle: {
    marginBottom: height * 0.07, 
  },
  answerText: {
    color:'white',
    textAlign: 'center',
    fontSize: 20
  },
  questionStyle: {
    color:'black',
    textAlign: 'center',
    fontSize: 20
  },
});
